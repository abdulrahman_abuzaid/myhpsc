
"""
Demonstration module for quadratic interpolation.
Update this docstring to describe your code.
Modified by: Abdulrahman Abuzaid
"""


import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import solve

def quad_interp(xi,yi):
    """
    Quadratic interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2.

    """

    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 3"
    assert len(xi)==3 and len(yi)==3, error_message

    # Set up linear system to interpolate through data points:

    A = np.array([np.ones(3),xi,xi**2]).T
    b = yi

    c = solve(A,b)


    return c

def cubic_interp(xi,yi):
    """
    Cubic interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2,3.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2 + c[3]*x**3.

    """

    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 4"
    assert len(xi)==4 and len(yi)==4, error_message

    # Set up linear system to interpolate through data points:

    A = np.array([np.ones(4),xi,xi**2,xi**3]).T
    b = yi

    c = solve(A,b)


    return c

def poly_interp(xi,yi):

    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have the same length"
    assert len(xi)==len(yi), error_message

    n = len(xi)
    A = np.array(np.ones(n))
    for i in range(1,n):
	A=np.c_[A,xi**(i)]
    b = yi

    c = solve(A,b)

    return c


def plot_quad(xi,yi):

    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 3"
    assert len(xi)==3 and len(yi)==3, error_message
    c = quad_interp(xi,yi)
    x = np.linspace(xi.min()-1,xi.max()+1,1000)
    y = c[0]+c[1]*x+c[2]*x**2
    plt.figure(1)
    plt.clf()
    plt.plot(x,y,'b-')

    plt.plot(xi,yi,'ro')
    #plt.ylim(yi.min()-4,yi.max()+4)

    plt.title("Data points and interpolating polynomial")
    plt.savefig('quadratic.png')
    
def plot_cubic(xi,yi):

    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 3"
    assert len(xi)==4 and len(yi)==4, error_message

    c = cubic_interp(xi,yi)
    x = np.linspace(xi.min()-1,xi.max()+1,1000)
    y = c[0]+c[1]*x+c[2]*x**2+c[3]*x**3
    plt.figure(1)
    plt.clf()
    plt.plot(x,y,'b-')

    plt.plot(xi,yi,'ro')
    #plt.ylim(yi.min()-4,yi.max()+4)

    plt.title("Data points and interpolating polynomial")
    plt.savefig('cubic.png')

def plot_poly(xi,yi):

    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have the same length"
    assert len(xi)==len(yi), error_message

    c = poly_interp(xi,yi)
    x = np.linspace(xi.min()-1,xi.max()+1,1000)
    n = len(xi)
    y = c[n-1]
    for j in range(n-1, 0, -1):
    	y = y*x + c[j-1]
    plt.figure(1)
    plt.clf()
    plt.plot(x,y,'b-')

    plt.plot(xi,yi,'ro')
    #plt.ylim(yi.min()-8,yi.max()+8)

    plt.title("Data points and interpolating polynomial")
    plt.savefig('polynomial.png')

def test_quad1():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([-1.,  0.,  2.])
    yi = np.array([ 1., -1.,  7.])
    c = quad_interp(xi,yi)
    c_true = np.array([-1.,  0.,  2.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)
        
def test_quad2():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([3.,  -1.,  4.])
    yi = np.array([ 11., 3.,  18.])
    c = quad_interp(xi,yi)
    c_true = np.array([2.,  0.,  1.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)

def test_cubic1():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([3, 1., 2., -1])
    yi = np.array([-20., 0., -5., 4.])
    c = cubic_interp(xi,yi)
    c_true = np.array([1.,  -1.,  1., -1.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)

def test_poly1():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([0., 1., -1., 2.])
    yi = np.array([3., 8., -4., 35.])
    c = poly_interp(xi,yi)
    c_true = np.array([3., 2., -1., 4.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)

def test_poly2():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([0., -1., 1., 5., -2.])
    yi = np.array([-2., 3., -1., 303., 44.])
    c = poly_interp(xi,yi)
    c_true = np.array([-2., 1., 2., -3., 1.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)


if __name__=="__main__":
    # "main program"
    # the code below is executed only if the module is executed at the command line,
    #    $ python demo2.py
    # or run from within Python, e.g. in IPython with
    #    In[ ]:  run demo2
    # not if the module is imported.
    print "Running test..."
    test_quad1()
    print
    test_quad2()
    print
    test_cubic1()
    print
    test_poly1()
    print 
    test_poly2()

