from newton import solve
from numpy import linspace, pi, cos, sin, array, zeros
import matplotlib.pyplot as plt

def fvals(x):
    	"""
    	Return f(x) and f'(x) for applying Newton to find the roots.
    	"""
    	f = x*cos(pi*x)+0.6*x**2-1
    	fp = cos(pi*x)-pi*x*sin(pi*x)+1.2*x
    	return f, fp

def g1(x):
	return x*cos(pi*x)

def g2(x):
	return 1-0.6*x**2

roots = array(zeros(4))
guesses = (-2.3,-1.6,-0.8,1.5)
n = 1001

for i in guesses:
	roots[i],iters = solve(fvals, i, False)

y1 = array(zeros(n))
y2 = array(zeros(n))
y3 = array(zeros(4))
x = linspace(-5,5,n)
y1 = g1(x)
y2 = g2(x)
y3 = g2(roots)
plt.figure(1)
plt.clf()
plt.plot(x,y1,'b-')
plt.plot(x,y2,'r-')
plt.ylim(-5,5) 
plt.legend(['g1','g2'])
plt.plot(roots,y3,'ko')
plt.savefig('intersections.png')
