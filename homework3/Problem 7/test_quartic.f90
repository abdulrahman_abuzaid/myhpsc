! $UWHPSC/codes/fortran/newton/test1.f90

program test1

    use newton, only: solve, tol
    use functions, only: f_quartic, fprime_quartic, epsilon

    implicit none
    real(kind=8) :: x, x0, fx, xstar
    real(kind=8) :: eps_vec(3), tol_vec(3)
    integer :: iters, itest, i, j
    logical :: debug = .false.         ! set to .true. or .false.

    x0 = 4.d0
    print *, ' '
    print 10, x0
10  format('Starting with initial guess ', es22.15)
    print *, ' '
    
    
    
    eps_vec = (/1d-04, 1d-08, 1d-12/)
    tol_vec = (/1d-05, 1d-10, 1d-14/)
    print *, '    epsilon        tol    iters          x                 f(x)        x-xstar'
    do i=1,3
        epsilon = eps_vec(i)
	xstar = 1+epsilon**(1.d0/4)
	print *, ' '  ! blank line
	do j=1,3
	    tol = tol_vec(j)
            
            call solve(f_quartic, fprime_quartic, x0, x, iters, debug)
	    fx = f_quartic(x)
            print 11, epsilon, tol, iters, x, fx, x-xstar
11          format(2es13.3, i4, es24.15, 2es13.3)
            enddo
	enddo
    print *, ' '

end program test1
