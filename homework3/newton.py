"""
Implementation of Newton's method in Python
By: Abdulrahman Abuzaid
"""

def solve(fvals, x0, debug):
    """
    Contains the main Newton interation
    """
    max_iter = 20
    x = x0
    tol = 10.**-14
    f, fp = fvals(x)
    k = 0
    if debug == True:
	print "Initial guess: x = %22.15e" % x
    while (abs(f) > tol and k < max_iter):
	x = x - f/fp
	k += 1
	f, fp = fvals(x)
	if debug == True:
	    print "After %i iterations, x = %22.15e" % (k, x)
    if k == max_iter and abs(f)>tol:
	print "*** Warning: has not converged yet"
    return x, k
    

def fvals_sqrt(x):
    """
    Return f(x) and f'(x) for applying Newton to find a square root.
    """
    f = x**2 - 4.
    fp = 2.*x
    return f, fp

def test1(debug_solve=False):
    """
    Test Newton iteration for the square root with different initial
    conditions.
    """
    from numpy import sqrt
    for x0 in [1., 2., 100.]:
        print " "  # blank line
        x,iters = solve(fvals_sqrt, x0, debug=debug_solve)
        print "solve returns x = %22.15e after %i iterations " % (x,iters)
        fx,fpx = fvals_sqrt(x)
        print "the value of f(x) is %22.15e" % fx
        assert abs(x-2.) < 1e-14, "*** Unexpected result: x = %22.15e"  % x
