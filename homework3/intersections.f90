! $MYHPSC/homework3/intersections.f90

program intersections

    use newton, only: solve
    use functions, only: f_int, fprime_int

    implicit none
    real(kind=8) :: x, x0, fx
    real(kind=8) :: x0vals(4)
    integer :: iters, itest
    logical :: debug = .true.

    print *, "routine to find the intersections"

    x0vals = (/-2.3, -1.6, -0.8, 1.5/)

    do itest = 1, 4
	x0 = x0vals(itest)
        print *, ' '
	call solve(f_int, fprime_int, x0, x, iters, debug)

	print 11, x, iters
11	format('solver returns x = ', es22.15, ' after', i3, ' iterations')

	fx = f_int(x)
	print 12, fx
12	format('the value of f(x) is ', es22.15)

	enddo

end program intersections
