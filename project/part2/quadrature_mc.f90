module quadrature_mc

    implicit none

contains

    function quad_mc(g, a, b, ndim, npoints)

    implicit none
    integer, intent(in) :: npoints, ndim
    real(kind=8), intent(in) :: a(ndim), b(ndim)
    real(kind=8), external :: g
    

    real(kind=8) :: quad_mc, volume, sum
    integer :: i
    real(kind=8) :: rand_array(ndim, npoints)

    call random_number(rand_array)

    quad_mc = 0
    do i=1,npoints
        rand_array(:,i) = a+rand_array(:,i)*(b-a)
        quad_mc = quad_mc + g(rand_array(:,i),ndim)
        enddo

    volume = product(b-a)
    quad_mc = quad_mc * volume / npoints   

    end function quad_mc

end module quadrature_mc
