
from pylab import *

# read in three columns from file and unpack into 3 arrays:
n,u_approx,error = loadtxt('mc_laplace_error.txt',unpack=True)

figure(1)
clf()
loglog(n,error,'-o',label='Monte-Carlo')
legend()
xlabel('number of MC points used')
ylabel('abs(error)')
title('Log-log plot of relative error in MC laplace')
savefig('mc_laplace_error.png')
