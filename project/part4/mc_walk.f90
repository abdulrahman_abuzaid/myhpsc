module mc_walk

    use mpi
    use random_util, only: init_random_seed 
    use problem_description

    integer :: nwalks, seed1
    
contains

subroutine random_walk(i0, j0, max_steps, ub, iabort)

    implicit none
    integer, intent(in) :: i0, j0, max_steps
    integer, intent(out) :: iabort
    real(kind=8), intent(out) :: ub

    real(kind=4) :: rnd(max_steps)
    integer :: i, j, k

    
    call random_number(rnd)

    i = i0
    j = j0
    do k = 1, max_steps
        if (rnd(k) .lt. 0.25) then
            i = i-1
        elseif (rnd(k) .lt. 0.5) then
            i = i+1
        elseif (rnd(k) .lt. 0.75) then
            j = j-1
        else
            j = j+1
        endif

        if (i*j*(nx+1-i)*(ny+1-j) .eq. 0) then
            ub = uboundary(ax+i*dx, ay+j*dy)
            iabort = 0
            go to 10
            endif

        if (k .eq. max_steps) then
            iabort = 1
            endif
 
        enddo

10 end subroutine random_walk

subroutine many_walks(i0, j0, max_steps, n_mc, u_mc, n_success)

    implicit none
    integer, intent(in) :: i0, j0, max_steps, n_mc
    integer, intent(out) :: n_success
    real(kind=8), intent(out) :: u_mc

    integer :: k, iabort, num_procs, proc_num, ierr, num_sent, sender
    integer, dimension(MPI_STATUS_SIZE) :: status
    real(kind=8) :: ub, ub_sum 

    call MPI_COMM_RANK(MPI_COMM_WORLD, proc_num, ierr)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, num_procs, ierr)
    
    ub_sum = 0
    n_success = 0

    if (proc_num == 0) then
        num_sent =0
        do k = 1, min(n_mc, num_procs-1)
            call MPI_SEND(MPI_BOTTOM, 0, MPI_DOUBLE_PRECISION, &
                          k, 1, MPI_COMM_WORLD, ierr)
            num_sent = num_sent + 1
            enddo
        do k = 1, n_mc
            call MPI_RECV(ub, 1, MPI_DOUBLE_PRECISION, &
                          MPI_ANY_SOURCE, MPI_ANY_TAG, &
                          MPI_COMM_WORLD, status, ierr)
            iabort = status(MPI_TAG)
            sender = status(MPI_SOURCE)
            if (iabort .eq. 0) then
                ub_sum = ub_sum+ub
                n_success = n_success+1
                endif
            if (num_sent < n_mc) then
                call MPI_SEND(MPI_BOTTOM, 0, MPI_DOUBLE_PRECISION, &
                              sender, 1, MPI_COMM_WORLD, ierr)
                num_sent = num_sent + 1
                else
                call MPI_SEND(MPI_BOTTOM, 0, MPI_DOUBLE_PRECISION, &
                              sender, 0, MPI_COMM_WORLD, ierr)
                endif
            enddo
        u_mc = ub_sum/n_success
        endif
            
    if (proc_num /= 0) then
        if(proc_num > n_mc) go to 99
        do while(.true.)
            call MPI_RECV(MPI_BOTTOM, 0, MPI_DOUBLE_PRECISION, 0, &
                          MPI_ANY_TAG, MPI_COMM_WORLD, status, ierr)
            k = status(MPI_TAG)
            if (k == 0) go to 99
            call random_walk(i0, j0, max_steps, ub, iabort)
            nwalks = nwalks+1
            call MPI_SEND(ub, 1, MPI_DOUBLE_PRECISION, 0, &
                          iabort, MPI_COMM_WORLD, ierr)

            enddo
        endif

99  continue

end subroutine many_walks


end module mc_walk
