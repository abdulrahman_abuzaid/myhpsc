module mc_walk

    use random_util, only: init_random_seed 
    use problem_description

    integer :: nwalks, seed1
    
contains

subroutine random_walk(i0, j0, max_steps, ub, iabort)

    implicit none
    integer, intent(in) :: i0, j0, max_steps
    integer, intent(out) :: iabort
    real(kind=8), intent(out) :: ub

    real(kind=4) :: rnd(max_steps)
    integer :: i, j, k

    
    call random_number(rnd)

    i = i0
    j = j0
    do k = 1, max_steps
        if (rnd(k) .lt. 0.25) then
            i = i-1
        elseif (rnd(k) .lt. 0.5) then
            i = i+1
        elseif (rnd(k) .lt. 0.75) then
            j = j-1
        else
            j = j+1
        endif

        if (i*j*(nx+1-i)*(ny+1-j) .eq. 0) then
            ub = uboundary(ax+i*dx, ay+j*dy)
            iabort = 0
            go to 10
            endif

        if (k .eq. max_steps) then
            iabort = 1
            endif
 
        enddo

10 end subroutine random_walk

subroutine many_walks(i0, j0, max_steps, n_mc, u_mc, n_success)

    implicit none
    integer, intent(in) :: i0, j0, max_steps, n_mc
    integer, intent(out) :: n_success
    real(kind=8), intent(out) :: u_mc

    integer :: k, iabort
    real(kind=8) :: ub, ub_sum 

    ub_sum = 0
    n_success = 0

    do k = 1, n_mc
        call random_walk(i0, j0, max_steps, ub, iabort)
        nwalks = nwalks+1
        if (iabort .eq. 0) then
            ub_sum = ub_sum+ub
            n_success = n_success+1
            endif
        enddo

    u_mc = ub_sum/n_success

end subroutine many_walks


end module mc_walk
