
program laplace_mc

use problem_description
use random_util, only: init_random_seed
use mc_walk, only: many_walks, random_walk, nwalks, seed1

implicit none
real(kind=8) :: x0, y0, u_true, u_mc, u_mc_old, error
integer :: k, i0, j0, max_steps, n_mc, n_success, n_success_total

open(unit=25, file='mc_laplace_error.txt', status='unknown')
seed1 = 12345
call init_random_seed(seed1)

x0 = 0.9
y0 = 0.6

i0 = nint((x0-ax)/dx)
j0 = nint((y0-ay)/dy)

x0 = ax+i0*dx
y0 = ay+j0*dy

u_true = utrue(x0,y0)
max_steps = 100*max(nx,ny)

nwalks = 0
n_mc = 10

call many_walks(i0, j0, max_steps, n_mc, u_mc, n_success)

error = abs((u_mc-u_true)/u_true)
print '(i8,e23.15,e15.6)', n_success, u_mc, error
write(25,'(i10,e23.15,e15.6)') n_success, u_mc, error


n_success_total = n_success
do k = 1, 12
    u_mc_old = u_mc
    call many_walks(i0, j0, max_steps, n_mc, u_mc, n_success)
    u_mc = (u_mc*n_success+u_mc_old*n_success_total)/(n_success+n_success_total)
    n_success_total = n_success_total + n_success
    error = abs((u_mc-u_true)/u_true)
    print '(i8,e23.15,e15.6)', n_success_total, u_mc, error
    write(25,'(i10,e23.15,e15.6)') n_success_total, u_mc, error
    n_mc = 2*n_mc
    enddo

print '("Final approximation to u(",f4.2,",",f4.2,"): ",3e23.15)', x0, y0, u_mc
print '("Total number of random walks: ",i8)', nwalks

end program laplace_mc
