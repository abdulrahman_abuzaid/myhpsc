! $MYHPSC/homework4/quadrature.f90

module quadrature

    ! module parameters
    implicit none



contains

subroutine trapezoid(f, a, b, n, int_trap)
    
    ! Estimate the integral of a function
    ! Input: 
    !   f: the function to integrate
    !   a: the lower limit of integration
    !   b: the higher limit of integration
    ! Returns:
    !   int_trap: the approximate integral of the function f

    implicit none
    real(kind=8), intent(in) :: a, b
    real(kind=8), external :: f
    real(kind=8), intent(out) :: int_trap
    integer, intent(in) :: n

    ! loclal variables
    real(kind=8) :: h
    integer :: k
    real(kind=8), dimension(n) :: x, fj

    h = (b-a)/(n-1)
    int_trap = 0
    do k=1,n
        x(k) = a + (k-1)*h
        fj(k) = f(x(k))
        int_trap = int_trap + fj(k)
        end do

    int_trap = h*(int_trap - 0.5d0*(fj(1)+fj(n)))
        
end subroutine trapezoid

subroutine error_table(f, a, b, nvals, int_true)

    ! Estimate the integral of a function with different numbers of internal points n 
    ! Input: 
    !   f: the function to integrate
    !   a: the lower limit of integration
    !   b: the higher limit of integration
    !   int_true: to help calculate the error
    !   nvals: an array containing the numbers of internal points to use trapezoidal with 

    implicit none
    real(kind=8), intent(in) :: a, b, int_true
    real(kind=8), external :: f
    integer, dimension(:), intent(in) :: nvals
    
    ! local variables
    integer :: nvals_size 
    integer :: k
    real(kind=8) :: int_trap, error, last_error, ratio

    nvals_size = size(nvals)
    last_error = 0.d0
    print *, "     n        trapezoid            error       ratio"
    do k=1,nvals_size
        call trapezoid(f,a,b,nvals(k),int_trap)
        error = abs(int_trap - int_true)
        ratio = last_error/error
        last_error = error
        print 11, nvals(k), int_trap, error, ratio
    11  format(i8, es22.14, es13.3, es13.3)
    end do

    print *, " " ! empty line

end subroutine error_table

end module quadrature
